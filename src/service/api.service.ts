import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url = 'localhost:27017/';

  constructor(private http: HttpClient) { }

  public get(resource) {
    return this.http.get(`${this.url}${resource}`);
  }

  public save(resource, data) {
    return this.http.post(`${this.url}${resource}`, data);
  }
}
