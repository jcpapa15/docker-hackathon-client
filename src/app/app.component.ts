import { Component } from '@angular/core';
import { BulletinBoard } from 'src/assets/model/bulletin-board';
import { ApiService } from 'src/service/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  bulletinBoard: BulletinBoard = new BulletinBoard();
  board = [];
  
  constructor(private apiService: ApiService) {}

  addToBoard(): void {
    this.apiService.save('', this.bulletinBoard).subscribe(
      data => {
        console.log(data);
        this.board.push(this.bulletinBoard);
        this.bulletinBoard = new BulletinBoard();
      }
    );
  }
}
